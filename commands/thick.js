const Discord = require('discord.js');
const ytdl = require('ytdl-core');
const streamOptions = { seek: 0, volume: 1 };
module.exports = {
	name: 'thickboi',
	description: 'Damn boi, fantano is thick 😤🤤😛!',
	aliases: ['thick', 'fantano', 'tb', 'boi'],
	usage: '',
	cooldown: 20,
	guildOnly: true,
	async execute(message, args) {
		if (message.member.voiceChannel) {
	      	const connection = await message.member.voiceChannel.join()
				.then(connection => {
					const stream = ytdl('https://www.youtube.com/watch?v=HMKUlsJpov8', { filter : 'audioonly' });
					const dispatcher = connection.playStream(stream, streamOptions);
					dispatcher.on('end', () => {
						connection.disconnect();
					});
				})
				.catch(console.error);

	    } else {
	      	message.reply('You need to join a voice channel first!');
	    }
	},
};
