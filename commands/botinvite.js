module.exports = {
	name: 'bot',
	description: 'Want to invite the bot to your own or a server you\'re admin on? Use this command to get an invite link',
	aliases: ['botinvite', 'binvite'],
	usage: '',
	cooldown: 20,
	guildOnly: false,
	execute(message, args) {
		message.channel.send('Thanks for liking the bot! ❤❤\n Here\'s a link: https://discordapp.com/oauth2/authorize?client_id=493389510944620545&scope=bot&permissions=37014528');
	},
};
