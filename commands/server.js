const Discord = require('discord.js');
module.exports = {
	name: 'server',
	description: 'Serves all the information you need about the server.',
	aliases: ['s', 'serverinfo', 'servinfo'],
	usage: '',
	cooldown: 5,
	guildOnly: true,
	execute(message, args) {
		if(`${message.guild.iconURL}` == 'null'){
			const number = Math.floor(Math.random() * 8);
			const serverInfoEmbed = new Discord.RichEmbed()
				.attachFiles(['./assets/mike'+number+'.png'])
			    .setColor('#6DE829')
			    .setTitle('Server Info')
			    .setAuthor('Mike Wazowski', 'attachment://mike'+number+'.png', 'https://gejr.dk/')
				.setThumbnail('attachment://mike'+number+'.png')
			    .addField('Server name', `${message.guild.name}`, true)
			    .addField('Member count', `${message.guild.memberCount}`, true)
				.addField('Region', `${message.guild.region}`, true)
			    .addField('Server owner', `${message.guild.owner}`, true)
				.addBlankField()
			    .setTimestamp()
			    .setFooter(`Mike Wazowski @ ${message.guild.name}`);
				message.channel.send(serverInfoEmbed);
		}
		else {
			const number = Math.floor(Math.random() * 8);
			const serverInfoEmbed = new Discord.RichEmbed()
				.attachFiles(['./assets/mike'+number+'.png'])
			    .setColor('#6DE829')
			    .setTitle('Server Info')
			    .setAuthor('Mike Wazowski', 'attachment://mike'+number+'.png', 'https://gejr.dk/')
			    .setThumbnail(`${message.guild.iconURL}`)
			    .addField('Server name', `${message.guild.name}`, true)
			    .addField('Member count', `${message.guild.memberCount}`, true)
				.addField('Region', `${message.guild.region}`, true)
			    .addField('Server owner', `${message.guild.owner}`, true)
				.addBlankField()
			    .setTimestamp()
			    .setFooter(`Mike Wazowski @ ${message.guild.name}`);
				message.channel.send(serverInfoEmbed);
		}
	},
};
