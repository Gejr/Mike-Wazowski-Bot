const Discord = require('discord.js');
module.exports = {
	name: 'ping',
	description: 'Ping Pong!',
	aliases: ['pingpong', 'pong'],
	usage: '',
	cooldown: 2,
	guildOnly: false,
	async execute(message, args) {
		// message.channel.send('Pong!');
		const m = await message.channel.send("Pinging?");
    	// m.edit(`Pong! Latency is ${m.createdTimestamp - message.createdTimestamp}ms. API Latency is ${Math.round(message.client.ping)}ms`);
		m.edit(new Discord.RichEmbed()
			.setColor('#6DE829')
			.setTitle(`**Pong!**\nLatency ${m.createdTimestamp - message.createdTimestamp}ms\nAPI Latency ${Math.round(message.client.ping)}ms`)
			.setTimestamp()
			.setFooter(`Mike Wazowski @ ${message.guild.name}`));
	},
};
