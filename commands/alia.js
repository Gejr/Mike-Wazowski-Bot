const Discord = require('discord.js');
const ytdl = require('ytdl-core');
const streamOptions = { seek: 0, volume: 1 };
module.exports = {
	name: 'alia',
	description: 'dun dun dun dun DUNDUNDUNDUNDUNDUNDUNDUN!',
	aliases: ['ali-a', 'ali', 'aa'],
	usage: '',
	cooldown: 7,
	guildOnly: true,
	async execute(message, args) {
		if (message.member.voiceChannel) {
	      	const connection = await message.member.voiceChannel.join()
				.then(connection => {
					const stream = ytdl('https://www.youtube.com/watch?v=CHD8cWEVVcY', { filter : 'audioonly' });
					const dispatcher = connection.playStream(stream, streamOptions);
					dispatcher.on('end', () => {
						connection.disconnect();
					});
				})
				.catch(console.error);

	    } else {
	      	message.reply('You need to join a voice channel first!');
	    }
	},
};
