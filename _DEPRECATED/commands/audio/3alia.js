const commando = require('discord.js-commando');
var isReady = true;
module.exports = class AliACommand extends commando.Command {
	constructor(client) {
		super(client, {
			name: 'alia',
			group: 'audio',
			memberName: 'ali',
			description: 'Ali-A intro #jamming',
			examples: [ ':mega:' ]
		});
	}

	async run(message, args) {
		console.log("command run - alia");
		if(isReady)
		{
			isReady = false;
			var voiceChannel = message.member.voiceChannel;
			if(voiceChannel != null)
			{
				voiceChannel.join().then(connection => {
					const dispatcher = connection.playFile('./audio/alia.ogg');
					console.log("playing audio...");
					dispatcher.on("end", end => {
						voiceChannel.leave();
					});
				}).catch(err => console.log(err));
			}
			else{
				message.reply("you need to be in a voice channel");
			}
			isReady = true;
		}
	}
}
