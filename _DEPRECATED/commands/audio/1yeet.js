const commando = require('discord.js-commando');
var isReady = true;
module.exports = class YeetCommand extends commando.Command {
	constructor(client) {
		super(client, {
			name: 'yeet',
			group: 'audio',
			memberName: 'yeet',
			description: 'let me yeet!',
			examples: [ ':mega:' ]
		});
	}

	async run(message, args) {
		console.log("command run - yeet");
		if(isReady)
		{
			isReady = false;
			var voiceChannel = message.member.voiceChannel;
			if(voiceChannel != null)
			{
				voiceChannel.join().then(connection => {
					const dispatcher = connection.playFile('./audio/yeet.ogg');
					console.log("playing audio...");
					dispatcher.on("end", end => {
						voiceChannel.leave();
					});
				}).catch(err => console.log(err));
			}
			else{
				message.reply("you need to be in a voice channel");
			}
			isReady = true;
		}
	}
}
