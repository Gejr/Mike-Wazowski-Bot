const { oneLine } = require('common-tags');
const commando = require('discord.js-commando');

module.exports = class BingCommand extends commando.Command {
	constructor(client) {
		super(client, {
			name: 'bing',
			group: 'util',
			memberName: 'bing',
			description: 'Checks the bot\'s :b:ing to the Discord server.',
			examples: [ ':ping_pong:' ],
			throttling: {
				usages: 5,
				duration: 10
			}
		});
	}

	async run(msg) {
		console.log("command run - bing");
		if(!msg.editable) {
			const pingMsg = await msg.reply(':b:inging...');
			return pingMsg.edit(oneLine`
				${msg.channel.type !== 'dm' ? `${msg.author},` : ''}
				:b:ong! The message round-trip took ${pingMsg.createdTimestamp - msg.createdTimestamp}ms.
				${this.client.ping ? `The heartbeat :b:ing is ${Math.round(this.client.ping)}ms.` : ''}
			`);
		} else {
			await msg.edit(':b:inging...');
			return msg.edit(oneLine`
				:b:ong! The message round-trip took ${msg.editedTimestamp - msg.createdTimestamp}ms.
				${this.client.ping ? `The heartbeat :b:ing is ${Math.round(this.client.ping)}ms.` : ''}
			`);
		}
	}
};
