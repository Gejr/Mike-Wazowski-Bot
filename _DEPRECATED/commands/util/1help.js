const commando = require('discord.js-commando');
const { stripIndents, oneLine } = require('common-tags');

module.exports = class HelpCommand extends commando.Command {
	constructor(client) {
		super(client, {
			name: 'help',
			group: 'util',
			memberName: 'help',
			description: 'Displays a list of available commands, or detailed information for a specified command.',
			examples: [ ':gear:' ]
		});
	}

	async run(msg, args) {
		console.log("command run - help");
		const commands = this.client.registry.findCommands(args.command, false, msg)
		msg.reply("Hey champ here is what you can do! \n" +
					  "\n" +
					  "	:ping_pong: " + this.client.commandPrefix + `${commands[1].name}` + " - "+ `${commands[1].description}` + "\n" +
					  `	${commands[9].examples}`+ " " + this.client.commandPrefix + `${commands[9].name}` + " - "+ `${commands[9].description}` + "\n" +		// yeet
					  `	${commands[10].examples}`+ " " + this.client.commandPrefix + `${commands[10].name}` + " - "+ `${commands[10].description}` + "\n" +
					  `	${commands[11].examples}`+ " " + this.client.commandPrefix + `${commands[11].name}` + " - "+ `${commands[11].description}` + "\n" +
					  `	${commands[12].examples}`+ " " + this.client.commandPrefix + `${commands[12].name}` + " - "+ `${commands[12].description}` + "\n" +
					  `	${commands[13].examples}`+ " " + this.client.commandPrefix + `${commands[13].name}` + " - "+ `${commands[13].description}` + "\n" +
					  `	${commands[14].examples}`+ " " + this.client.commandPrefix + `${commands[14].name}` + " - "+ `${commands[14].description}` + "\n" +
					  `	${commands[15].examples}`+ " " + this.client.commandPrefix + `${commands[15].name}` + " - "+ `${commands[15].description}` + "\n" +
					  "\n" //+
					  // "Values in [square brackets] are optional.\n"+
					  // "Values in <angle brackets> have to be provided by you.\n"+
					  // "Values in 'quotes' are literal.\n"+
					  // "The | sign means one or the other."
	);
	}
}
