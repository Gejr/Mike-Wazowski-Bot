const commando = require('discord.js-commando');

module.exports = class DiceRollCommand extends commando.Command {
	constructor(client) {
		super(client, {
			name: 'roll',
			group: 'random',
			memberName: 'roll',
			description: 'Rolls a die.',
			examples: [ ':game_die:' ]
		});
	}

	async run(message, args) {
		console.log("command run - roll");
		var roll = Math.floor(Math.random() * 6) + 1;
		message.reply("You rolled a " + roll);
	}
}
