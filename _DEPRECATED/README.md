# Mike Wazowski Bot
A discord.js bot, named Mike Wazowski after the iconic and greatest character ever to be made!

## How to install
Step 1) Use the command in your Git-CLI `git clone https://gitlab.com/Gejr/Mike-Wazowski-Bot.git`<br>
Step 2) Install `FFmpeg`, `NodeJS` & `NPM`<br>
Step 3) Navigate to the bot install folder<br>
Step 4) Install the bots dependencies with this command `npm install --save node-opus opusscript discord.js discord.js-commando sqlite`<br>
Step 5) Start the bot with this command `node --harmony .`<br>
